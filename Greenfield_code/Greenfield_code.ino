/*
 GreenField 
  |--> device that monitors the temperature, 
       noise and carbon dioxide levels via devicehub.net
 
 Hardware parts:
   1. Microcontroller board     ---> Arduino Ethernet
                                 |-> http://arduino.cc/en/Main/arduinoBoardEthernet
   2. Sound sensor              ---> Grove - Sound Sensor w/ LM358 amplifier and an electret microphone  
                                 |-> http://www.seeedstudio.com/wiki/index.php?title=Twig_-_Sound_Sensor
   3. Temperature sensor        ---> One Wire Temperature Sensor waterproof DS18B20
                                 |-> http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf     
   4. CO2 sensor                ---> Grove - CO2 Sensor
                                 |-> http://www.seeedstudio.com/wiki/images/c/ca/MH-Z16_CO2_datasheet_EN.pdf
   5. Additional parts          ---> 4.7K resistor
 
 Connections:
   1. Sound sensor              ---> Arduino Analog A0
   2. Temperature sensor        ---> Arduino Digital 2
                                 |-> 4.7K resistor between signal and the power pin
   3. CO2 sensor                ---> Arduino Digital 3 ( TX )
                                 |-> Arduino Digital 4 ( RX )
 Libraries:
   1. OneWire                   ---> http://code.bildr.org/download/997.zip
   2. MQTT PubSubClient         ---> https://github.com/knolleary/pubsubclient/archive/v1.9.1.zip
     
 Web platform:    
   devicehub.net                ---> profile:  constantin.craciun
                                ---> password: electric
                                ---> project:  GreenField
 20.11.2014                         
 Author: Alexandru Gheorghe
*/

#include <SPI.h>
#include <OneWire.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>

byte mac[]      = {  0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };
byte server[]   = { 176, 124, 106, 85 };    //server ip ( devicehub.net )
char clientId[]  = "ArduinoEthernetClient";

// Update these with values suitable for your devicehub.net project
#define API_KEY         "c7693fdb-4f0d-45f3-9216-3dc9223bd472"
#define PROJECT_ID      "516"
#define TEMPERATURE_ID  "962"
#define NOISE_ID        "963"
#define GAS_ID          "964"
#define noise_pin        A0
#define noise_pin_VCC    9
#define temperature_pin  2
#define pinTx            3
#define pinRx            4

// Noise variables
const int sampleWindow = 50; 
unsigned int sample;
const int numReadings = 10;
int readings[numReadings];      
int index = 0;                  
int total = 0;                  
int average = 0;                

char tempTopic[]        = "/"API_KEY"/"PROJECT_ID"/sensor/"TEMPERATURE_ID"";
char noiseTopic[]       = "/"API_KEY"/"PROJECT_ID"/sensor/"NOISE_ID"";
char gasTopic[]         = "/"API_KEY"/"PROJECT_ID"/sensor/"GAS_ID"";
boolean conn = false;

void callback(char* topic, byte* payload, unsigned int length){
  // handle message arrived
}

EthernetClient ethClient;
PubSubClient client(server, 1883, callback, ethClient);

long publishInterval = 5 * 1000;   // between updates to devicehub.net in miliseconds
unsigned lastPublishTime = 0;

OneWire ds(temperature_pin);           // temperature sensor

// AirQuality variables
SoftwareSerial sensor(pinTx,pinRx);
unsigned char flg_get = 0;                        
const unsigned char cmd_get_sensor[] = {
0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};
const unsigned char cmd_c_sensor[] = {
0xff, 0x01, 0x87, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78};

void setup()  {
  Serial.begin(115200);
  sensor.begin(9600);
  pinMode(noise_pin, INPUT); 
  pinMode(noise_pin_VCC, OUTPUT); 
  digitalWrite(noise_pin_VCC, HIGH);
  
  Serial.println("Starting...");
  Serial.println("Calibrating CO2 sensor...");
  
  for(int i=0; i<sizeof(cmd_c_sensor); i++)  {
    sensor.write(cmd_c_sensor[i]);
  }
  delay(5000);  
  
  Serial.println("Calibration done");
  
  if(Ethernet.begin(mac) == 0)  {
    Serial.println(F("Failed to get DHCP IP"));
  }

  delay(1000);

  if (client.connect(clientId))  {
    conn=true;
  }

  lastPublishTime = millis();
}

void loop()  {
  char tempValue[10];
  char noiseValue[10];
  char gasValue[10];
  
  float temperature = getTemp();
  int noise = getNoise();
  
  flg_get = 0;
  int gas;
  int temp;

  if(sendCmdGetDta(&gas, &temp))  {
    gas;
  }
  
  if(conn)  {
    if((lastPublishTime+publishInterval) < millis()) { 
      lastPublishTime = millis();
      // publishes "temperature" to the topic "tempTopic"
      dtostrf(temperature, 2, 2, tempValue);
      client.publish(tempTopic, (uint8_t*)tempValue, sizeof(tempValue));
      // publishes "dB" to the topic "noiseTopic"
      dtostrf(noise, 2, 2, noiseValue);
      client.publish(noiseTopic, (uint8_t*)noiseValue, sizeof(noiseValue));
      // publishes "CO2" to the topic "gasTopic"
      dtostrf(gas, 1, 2, gasValue);
      client.publish(gasTopic, (uint8_t*)gasValue, sizeof(gasValue));
      // For debug purposes
      // Serial.print("temperature: ");
      // Serial.print(temperature);
      // Serial.print(", noise: ");
      // Serial.print(noise);
      // Serial.print(", CO2 PPM: ");
      // Serial.println(gas);
      // Serial.println("*************"); 
    }
    client.loop();
  }
}

// returns the decibels from sound sensor
float getNoise()  {
  unsigned long startMillis= millis();  
  unsigned int peakToPeak = 0;   

  unsigned int signalMax = 0;
  unsigned int signalMin = 750;

  while (millis() - startMillis < sampleWindow)  {
    sample = analogRead(noise_pin);
    if (sample < 1024)  {
      if (sample > signalMax)  {
        signalMax = sample;  
      }else if (sample < signalMin)  {
         signalMin = sample;  
      }
    }
  }
  peakToPeak = signalMax - signalMin;  
  double db = 3.1 * 20.0  * log10 (peakToPeak  +1.);
   
  total= total - readings[index];         
  readings[index] = db; 
  total= total + readings[index];       
  index = index + 1;                    

  if (index >= numReadings) 
    index = 0;                           

  average = total / numReadings;         
  
  int x = map(average, 100, 190, 40,100);
  //average *= 1.1;
  delay(30);
  Serial.println(x);
  if(x > 37)
    return x;
}

// returns the temperature from DS18S20 in DEG Celsius
float getTemp()  {
  byte data[12];
  byte addr[8];

  if(!ds.search(addr))  {
    ds.reset_search();
    return -1000;
  }

  if (OneWire::crc8( addr, 7) != addr[7])  {
    return -1000;
  }

  if (addr[0] != 0x10 && addr[0] != 0x28)  {
    return -1000;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);

  byte present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);

  for(int i = 0; i < 9; i++)  {
    data[i] = ds.read();
  }

  ds.reset_search();

  byte MSB = data[1];
  byte LSB = data[0];

  float tempRead = ((MSB << 8) | LSB);
  float TemperatureSum = tempRead / 16;
  delay(1000);
  return TemperatureSum;
}

//returns the gas strength from the CO2 sensor
bool sendCmdGetDta(int *gas_strength, int *temperature)  {
  for(int i = 0; i < sizeof(cmd_get_sensor); i++)  {
    sensor.write(cmd_get_sensor[i]);
  }
  
  long cnt_timeout = 0;
  while(!sensor.available())  {
    delay(1);
    cnt_timeout++;    
    if(cnt_timeout > 1000)
      return 0;
  }
  
  int len = 0;
  unsigned char dta[20];
  
  while(sensor.available())  {
    dta[len++] = sensor.read();
  }
  
  if((9 == len) && (0xff == dta[0]) && (0x86 == dta[1])){
    *gas_strength = 256*dta[2]+dta[3];
    *temperature = dta[4] - 40;
    return 1;
  }
  return 0;  
}

void serialEvent()  {
  while (Serial.available())  {
    char c = Serial.read();
    if(c == 'g')flg_get = 1;
  }
}
